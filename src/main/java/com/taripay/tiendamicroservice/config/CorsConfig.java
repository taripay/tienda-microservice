package com.taripay.tiendamicroservice.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.CorsRegistry;
import org.springframework.web.reactive.config.WebFluxConfigurer;

@Configuration
public class CorsConfig implements WebFluxConfigurer {
    
    @Override
    public void addCorsMappings(CorsRegistry registry){
        registry.addMapping("/**")
                .allowedMethods("POST", "GET", "PUT", "PATCH", "DELETE")
                .allowedOriginPatterns("*")
                .allowCredentials(false);
    }
    
}
