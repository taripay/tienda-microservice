package com.taripay.tiendamicroservice.service;

import com.taripay.tiendamicroservice.dao.ProductoDTO;
import com.taripay.tiendamicroservice.entity.Tienda;
import com.taripay.tiendamicroservice.repository.TiendaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import com.taripay.tiendamicroservice.clients.WebClientConfig;
import org.springframework.web.reactive.function.client.WebClient;

/**
 *
 * @author roberth
 */
@Service
public class TiendaService {
    
    @Autowired
    TiendaRepository tiendaRepository;
    
    @Autowired
    WebClient webClient;
    
    public Mono<Tienda> save(Tienda model){
        return tiendaRepository.save(model);
    }
    
    public Flux<Tienda> getByUsuario(Long usuarioId){
        return tiendaRepository.findByUsuarioId(usuarioId);
    }
    
    public Flux<ProductoDTO> getProductosByTienda(Long tiendaId){
        return webClient.get()
                .uri("/api/producto/tienda/" + tiendaId)
                .retrieve()
                .bodyToFlux(ProductoDTO.class);
    }
}
