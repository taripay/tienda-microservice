package com.taripay.tiendamicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TiendaMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TiendaMicroserviceApplication.class, args);
	}

}
