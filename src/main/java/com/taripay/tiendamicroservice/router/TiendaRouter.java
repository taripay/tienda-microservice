package com.taripay.tiendamicroservice.router;

import com.taripay.tiendamicroservice.handler.TiendaHandler;
import org.springframework.boot.autoconfigure.web.WebProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

/**
 *
 * @author roberth
 */

@Configuration
public class TiendaRouter {
    
    private static String path = "/api/tienda";
    
    @Bean
    public WebProperties.Resources resources(){
        return new WebProperties.Resources();
    }
    
    @Bean
    RouterFunction<ServerResponse> routerTienda(TiendaHandler handler){
        return RouterFunctions.route()
                .GET(path + "/usuario/{usuarioId}", handler::getByUsuario)
                .GET(path + "/prop", handler::getTestProp)
                .GET(path + "/tienda/{tiendaId}", handler::getProducts)
                .POST(path, handler::save)
                .build();
    }
}
