/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.taripay.tiendamicroservice.dao;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

/**
 *
 * @author roberth
 */
@Getter
@Setter
public class ProductoDTO {
    private Long id;
    private String codigo;
    private String descripcion;
    private double precio;
    private int stock;
    private Long tiendaId;
}
