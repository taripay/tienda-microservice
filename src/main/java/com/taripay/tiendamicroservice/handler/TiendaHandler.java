package com.taripay.tiendamicroservice.handler;

import com.taripay.tiendamicroservice.dao.ProductoDTO;
import com.taripay.tiendamicroservice.entity.Tienda;
import com.taripay.tiendamicroservice.service.TiendaService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.MediaType;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

/**
 *
 * @author roberth
 */
@Component
@Getter
@Setter
@RefreshScope
public class TiendaHandler {
    
    private final MediaType typeJson = MediaType.APPLICATION_JSON;
    
    @Autowired
    TiendaService tiendaService;
    
    @Value("${app.testProp}")
    private String testProp;
    
    public Mono<ServerResponse> getByUsuario(ServerRequest request){
        Long usuarioId = Long.parseLong(request.pathVariable("usuarioId"));
        return ServerResponse.ok()
                .contentType(typeJson)
                .body(tiendaService.getByUsuario(usuarioId), Tienda.class);
    }
    
    public Mono<ServerResponse> save(ServerRequest request){
        Mono<Tienda> model = request.bodyToMono(Tienda.class);
        
        return model.flatMap( tienda -> ServerResponse.ok()
                .contentType(typeJson)
                .body(tiendaService.save(tienda), Tienda.class)
        );
    }
    
    public Mono<ServerResponse> getProducts(ServerRequest request){
        Long id = Long.parseLong(request.pathVariable("tiendaId"));
        return ServerResponse.ok()
                .contentType(typeJson)
                .body(tiendaService.getProductosByTienda(id), ProductoDTO.class);
    }
    
     public Mono<ServerResponse> getTestProp(ServerRequest request){

            return ServerResponse.ok()
                .contentType(typeJson)
                .body(Mono.just(this.testProp), String.class);
        } 
    
}
