/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.taripay.tiendamicroservice.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import com.taripay.tiendamicroservice.entity.Tienda;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
/**
 *
 * @author roberth
 */
@Repository
public interface TiendaRepository extends ReactiveCrudRepository<Tienda, Long>{
    
    public Flux<Tienda> findByUsuarioId(Long usuarioId);
}
