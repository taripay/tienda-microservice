package com.taripay.tiendamicroservice.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.relational.core.mapping.Table;

/**
 *
 * @author roberth
 */
@Table(value="tb_tienda")
@Getter
@Setter
@Builder
public class Tienda {
    private Long id;
    private String codigo;
    private String descripcion;
    private String direccion;
    private String latitud;
    private String longitud;
    private Long usuarioId;
}
