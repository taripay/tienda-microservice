package com.taripay.tiendamicroservice.clients;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

/**
 *
 * @author roberth
 */
@Configuration
public class WebClientConfig {
    
    private String baseUrl = "http://localhost:8080";
    
    @Bean
    public WebClient.Builder webClientBuilder(){
        return WebClient.builder();
    }
    
    @Bean
    public WebClient webClient(WebClient.Builder webClientBuilder){
        return webClientBuilder.baseUrl(baseUrl).build();
    }
    
    
    
}
